package com.bm;

import com.bm.jpa.Book;
import com.bm.jpa.BookRepository;
import com.bm.service.BookService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookMarketApplicationTests {

	@Autowired
	private WebApplicationContext wac;

	private RestTemplate restTemplate;
	private MockRestServiceServer mockServer;
	private MockMvc mockMvc;
	private MediaType applicationJsonMediaType = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
	private BookRepository bookRepository;
	private BookService bookService;
	@Before
	public void initD(){
		bookRepository = wac.getBean(BookRepository.class);
		bookService = wac.getBean(BookService.class);
		List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
		converters.add(new StringHttpMessageConverter());
		converters.add(new MappingJackson2HttpMessageConverter());

		this.restTemplate = new RestTemplate();
		this.restTemplate.setMessageConverters(converters);

		this.mockServer = MockRestServiceServer.createServer(this.restTemplate);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void contextLoads() {
	}


// BookRepository TESTS

	@Test
	public void findAllBy(){
		List<Book> books = bookRepository.findAllBy();
		Assert.assertNotNull(books);
		Assert.assertTrue(books.size() > 0);
	}

	@Test
	public void findById(){
		Book book = bookRepository.findById(1L);
		Assert.assertNotNull(book);
		Assert.assertTrue("Чистый код".equals(book.getBookName()));
	}

	@Test
	public void deleteById(){
		bookRepository.deleteById(2L);
		Book book = bookRepository.findById(2L);
		Assert.assertNull(book);
	}

	@Test
	public void save(){
//		'Чистый код', 'Роберт Мартин'
		Book book = new Book();
		book.setBookName("Чистый код2");
		book.setAuthor("Роберт Мартин2");
		book.setDescription("");
		Book book2 = bookRepository.save(book);
		book = bookRepository.findById(book2.getId());
		Assert.assertNotNull(book);
		Assert.assertTrue("Чистый код2".equals(book.getBookName()));
	}

// BookService TESTS

	@Test
	public void update(){
		Book book = bookService.findById(1L);
		book.setDescription("24-56-89");
		Book bookRezult = bookService.update(1L, book);
		Assert.assertNotNull(bookRezult);
		Assert.assertTrue("24-56-89".equals(bookRezult.getDescription()));
	}

//BookRestController TESTS


	@Test
	public void findByIdRest() throws Exception {
		this.mockMvc.perform(get("/book/1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(applicationJsonMediaType))
				.andExpect(jsonPath("$.bookName", is("Чистый код")));
	}

	@Test
	public void deleteByIdRest(){
		try {
			mockMvc.perform(delete("/book/{Id}", 3L)).andExpect(status().isOk());
		}catch (Exception e){
			Assert.fail(e.toString());
		}
	}


	@Test
	public void CreateBookRest(){
		try{
			String jsonOfJoeDoe = "{\"description\":\"\",\"author\":\"Томас Кормен, Чарльз Лейзерсон, Рональд Ривест и Клиффорд Штайн\",\"bookName\":\"Алгоритмы: построение и анализ\"}";

			MvcResult mvcResult = mockMvc.perform(post("/book/")
					.accept(applicationJsonMediaType)
					.content(jsonOfJoeDoe)
					.contentType(this.applicationJsonMediaType))
					.andReturn();
			mockServer.verify();
		}catch (Exception e){
			Assert.fail(e.toString());
		}
	}


	@Test
	public void EditBookRest(){
		try{
			String jsonOfJoeDoe = "{\"description\":\"24-57-80\"," +
					"\"author\":\"Кэти Сьерра и Берт Бейтс\",\"bookName\":\"Изучаем Java\"}";
			MvcResult mvcResult = mockMvc.perform(put("/book/5")
					.accept(applicationJsonMediaType)
					.content(jsonOfJoeDoe)
					.contentType(this.applicationJsonMediaType))
					.andReturn();

			mockServer.verify();
		}catch (Exception e){
			Assert.fail(e.toString());
		}
	}




}
