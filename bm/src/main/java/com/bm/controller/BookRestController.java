package com.bm.controller;


import com.bm.jpa.Book;
import com.bm.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/book")
public class BookRestController {

    @Autowired
    BookService bookService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Book findById(@PathVariable Long id){
        return bookService.findById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteById(@PathVariable Long id){
        bookService.deleteById(id);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public Book save(@RequestBody Book book){
        return bookService.save(book);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public Book update(@PathVariable Long id, @RequestBody Book book){
        return bookService.update(id, book);
    }
}
