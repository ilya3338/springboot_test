package com.bm.jpa;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Book implements Serializable {

    @Id
    @SequenceGenerator(name = "book_generator", sequenceName = "book_sequence", initialValue = 23)
    @GeneratedValue(generator = "book_generator")
    Long id;

    @Column(nullable = false)
    String Name;
    @Column(nullable = false)
    String Author;
    @Column(nullable = false)
    String Description;

    public Long getId() {
        return id;
    }

    public String getBookName() {
        return Name;
    }

    public void setBookName(String bookName) {
        Name = bookName;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    @Override
    public String toString() {
        return  id + ","+getBookName() + "," + getAuthor() + "," + getDescription();
    }
}
