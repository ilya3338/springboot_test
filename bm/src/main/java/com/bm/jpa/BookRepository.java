package com.bm.jpa;

import org.springframework.data.repository.Repository;

import java.util.List;

public interface BookRepository  extends Repository<Book, Long>{

    List<Book> findAllBy();
    Book findById(Long id);
    void deleteById(Long id);
    Book save(Book book);

}
