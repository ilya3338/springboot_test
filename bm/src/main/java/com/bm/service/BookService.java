package com.bm.service;

import com.bm.jpa.Book;

public interface BookService {

    Book findById(Long id);
    void deleteById(Long id);
    Book save(Book book);
    Book update(Long id, Book book);
}
