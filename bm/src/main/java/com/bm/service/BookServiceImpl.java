package com.bm.service;

import com.bm.jpa.Book;
import com.bm.jpa.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("BookServiceImpl")
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepository bookRepository;

    @Override
    public Book findById(Long id) {
        return bookRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) {
        bookRepository.deleteById(id);
    }

    @Override
    public Book save(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public Book update(Long id, Book book) {
        Book bookFind = bookRepository.findById(id);
        bookFind.setAuthor(book.getAuthor());
        bookFind.setBookName(book.getBookName());
        bookFind.setDescription(book.getDescription());
        return bookRepository.save(bookFind);
    }
}
